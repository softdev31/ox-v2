/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author ASUS
 */
public class TDDTest {
    
    public TDDTest() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void testAdd_1_2is3() {
        assertEquals(3, Example.add(1,2));
    }
    @Test
    public void testAdd_3_4is7() {
        assertEquals(7, Example.add(3,4));
    }
    
    @Test
    public void testAdd_20_22is42() {
        assertEquals(42, Example.add(20,22));
    }
    //p:paper,s:scissors,h:hammer
    //chup(char player1,char player2)->"p1","p2","draw"
    @Test
    public void testChop_p1_p_p2_p_is_draw(){
        assertEquals("draw",Example.chop('p','p') );
    }
    
    @Test
    public void testChop_p1_h_p2_h_is_draw(){
        assertEquals("draw",Example.chop('h','h') );
    }
    
    @Test
    public void testChop_p1_s_p2_s_is_draw(){
        assertEquals("draw",Example.chop('s','s') );
    }
    
    @Test
    public void testChop_p1_s_p2_p_is_p1(){
        assertEquals("p1",Example.chop('s','p') );
    }
    
    @Test
    public void testChop_p1_h_p2_s_is_p1(){
        assertEquals("p1",Example.chop('h','s') );
    }
    
    @Test
    public void testChop_p1_p_p2_h_is_p1(){
        assertEquals("p1",Example.chop('p','h') );
    }
    
    @Test
    public void testChop_p1_p_p2_s_is_p2(){
        assertEquals("p2",Example.chop('p','s') );
    }
    
    @Test
    public void testChop_p1_s_p2_h_is_p2(){
        assertEquals("p2",Example.chop('s','h') );
    }
    
    @Test
    public void testChop_p1_h_p2_p_is_p2(){
        assertEquals("p2",Example.chop('h','p') );
    }
}
