/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */

import com.mycompany.oxprogramv2.OXProgramV2;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author ASUS
 */
public class OXprogramTest {
    
    public OXprogramTest() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
     public void testCheckVerticlePlayerOCol1win() {
         char table[][] =  {{'O', '-', '-'}, 
                                {'O', '-', '-'}, 
                                {'O', '-', '-'}};
         char currentPlayer = 'O';
         int col = 1;
         assertEquals(true,OXProgramV2.checkVertical(table, currentPlayer, col));
     }
     
     @Test
     public void testCheckVerticlePlayerOCol2win() {
         char table[][] =  {{'-', 'O', '-'}, 
                                {'-', 'O', '-'}, 
                                {'-', 'O', '-'}};
         char currentPlayer = 'O';
         int col = 2;
         assertEquals(true,OXProgramV2.checkVertical(table, currentPlayer, col));
     }
     
     @Test
     public void testCheckVerticlePlayerOCol3win() {
         char table[][] =  {{'-', '-', 'O'}, 
                                {'-', '-', 'O'}, 
                                {'-', '-', 'O'}};
         char currentPlayer = 'O';
         int col = 3;
         assertEquals(true,OXProgramV2.checkVertical(table, currentPlayer, col));
     }
     
     @Test
     public void testCheckPlayerOCol3win() {
         char table[][] =  {{'O', '-', '-'}, 
                                {'-', 'O', '-'}, 
                                {'-', '-', 'O'}};
         char currentPlayer = 'O';
         int col = 3;
         int row = 3;
         assertEquals(true,OXProgramV2.checkWin(table, currentPlayer, row,col));
     }
     
     @Test
     public  void testDraw(){
         int count = 8;
         assertEquals(true,OXProgramV2.checkDraw(count));
     }
     
     @Test
     public void testcheckHorizontalPlayerOrow1win() {
         char table[][] =  {{'O', 'O', 'O'}, 
                                {'-', '-', '-'}, 
                                {'-', '-', '-'}};
         char currentPlayer = 'O';
         int row = 1;
         assertEquals(true,OXProgramV2.checkHorizontal(table, currentPlayer, row));
     }
     @Test
     public void testcheckHorizontalPlayerOrow2win() {
         char table[][] =  {{'-', '-', '-'}, 
                                {'O', 'O', 'O'}, 
                                {'-', '-', '-'}};
         char currentPlayer = 'O';
         int row = 2;
         assertEquals(true,OXProgramV2.checkHorizontal(table, currentPlayer, row));
     }
     @Test
     public void testcheckHorizontalPlayerOrow3win() {
         char table[][] =  {{'-', '-', '-'}, 
                                {'-', '-', '-'}, 
                                {'O', 'O', 'O'}};
         char currentPlayer = 'O';
         int row = 3;
         assertEquals(true,OXProgramV2.checkHorizontal(table, currentPlayer, row));
     }
     
     
     @Test
     public void testcheckXPlayerOwin() {
         char table[][] =  {{'-', '-', 'O'}, 
                                {'-', 'O', '-'}, 
                                {'O', '-', '-'}};
         char currentPlayer = 'O';
         assertEquals(true,OXProgramV2.checkX(table, currentPlayer));
     }
     
     @Test
     public void testcheckX1PlayerOwin() {
         char table[][] =  {{'O', '-', '-'}, 
                                {'-', 'O', '-'}, 
                                {'-', '-', 'O'}};
         char currentPlayer = 'O';
         assertEquals(true,OXProgramV2.checkX1(table, currentPlayer));
     }
     
}

